let w: unknown = 1;
w = "string";
w = {
    runANonExistedMethod: () => {
        console.log("I think therefore I am");
    }
} as {runANonExistedMethod: () => void }

if(typeof w === 'object' && w != null) {
    (w as {runANonExistedMethod: Function}).runANonExistedMethod();
}