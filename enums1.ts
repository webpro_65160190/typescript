enum CardinalDirection {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CardinalDirection.East;
console.log(currentDirection);
