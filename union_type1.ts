function printStatusCode(code: string|number){
    if(typeof code === 'string') {
        console.log(`My Status code is ${code.toUpperCase()} ${typeof code}`);
    }else{
        console.log(`My Status code is ${code} ${typeof code}`);
    }
}

printStatusCode(404);
printStatusCode("abc");
